from django.contrib import admin

from ads.ad.models import Ad
from ads.comment.models import Comment

admin.site.register(Ad)
admin.site.register(Comment)